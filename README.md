# **CastorEngine Version 5** #

### C’est quoi ? ###

[CastorEngine](http://www.castorengine.com/) est un éditeur de jeu 3D **gratuit** intégré au navigateur utilisant la technologie WebGL grâce au moteur 3d [BabylonJS](http://www.babylonjs.com/). 
Cet éditeur a été pensé pour les créateurs amateurs, particulier qui souhaitent créer des jeux vidéo 3D sans mettre les mains sur des logiciels difficiles à comprendre ou à prendre en main. Les utilisateurs avancées et professionnelle peuvent aussi l'utiliser et en tirer toutes sa puissance pour créer de grand jeux. Vous pouvez avec CastorEngine créer tout type de jeux (plateforme, FPS, RPG, multijoueur, MMO, click and points et autres...)

Voici l'interface à quoi elle ressemble:

![castorengine_new.jpg](https://bitbucket.org/repo/x5qk5M/images/2130533819-castorengine_new.jpg)

* Vous voulez m'aider et participer, partager vos contributions, cloner [le dépôt](https://bitbucket.org/Dad72/castorengine/src) dans: [sourceTree](http://www.sourcetreeapp.com/?utm_source=internal&utm_medium=link&utm_campaign=clone_repo_win)

**Pour ce qui est à venir et en cours, voir notre:** [**Feuille de route**](https://bitbucket.org/Dad72/castorengine/wiki/Feuille%20de%20route)

# Caractéristiques #

CastorEngine est un moteur de jeu 3D complet.

* Graphique
    * Base WebGL: moteur de rendu 3D Babylon.js
    * Lumière Directionnel, spots et hémisphérique (Directionnel, spots peuvent recevoir des ombres)   
    * moteur de particules avec l'éditeur
    * importation d'objets 3d, de scènes complètes, exportation des os, biped et de peau.
    * Soutien des animations avec images clés.
    * Soutient les standards matériels par défaut de 3ds Max et Blender.
    * Modèles et scènes complète exporté avec 3ds Max, Blende.
    * Support des texture tga et dds
* Physique
    * Moteur physique Oimo.js ou Cannon.js (intégrer dans Babylon.js)
* Collision
    * Moteur d'intersection d'objets sans recours à la physique (intégrer dans Babylon.js)
    * Scène picking pour sélectionner/cliquer des objets de la scène.
    * Collisions webworkers
* Performance
    * Instance
    * Octree
    * scène optimiseur
    * Niveau de détail (LOD)
* Musique et son
    * Moteur Audio positionnel 3D via Web Audio API (intégrer dans Babylon.js)
* Chargement des ressources
    * Simple et puissant chargement des ressources avec aperçue de modèles, scène, textures, musiques, son et vidéos
* Composants
    * videoTexture, audioSource, matériels, particules collisions, rigidbody, script, réseau, stikers, actions, texture procédural, lensflare...
    * Ajouter des caméras, objet primitifs (box, sphère, cylindre, torus, plan et eau), lumières, points, portail de téléportation et videoSource
* Système de script
    * Ecrire le comportement des jeux en attachant des scripts JavaScript sur les objets. (Écriture de scripts basés sur Babylon.js)
* Contrôles
    * Soutien la Souris, le clavier, Touch, Gamepad. (Hand.js)

Et bien d'autre caractéristique. Voir sur le site pour la liste complètes.

# License #

CastorEngine est publié sous la licence MIT. [Voir fichier de licence.](http://opensource.org/licenses/MIT)